const assert = require('assert');
const DTL = require("../lib/DTL.js");

describe('Bit Group and Remap Functions', function() {
    
    describe('bit_group function', function() {
        it("should correctly group bits from a Uint8Array", function() {
            const container = { input: { bytes: Array.from(new Uint8Array([104, 101, 108, 108, 111])) } };
            const transform = { out: "(: bit_group($input.bytes 5) :)" };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, [13, 1, 18, 22, 24, 27, 3, 15]);
        });

        it("should correctly group bits from a number", function() {
            const container = { input: { number: 1048576 } };
            const transform = { out: "(: bit_group($input.number 5) :)" };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, [0, 0, 8, 0, 0, 0, 0]);
        });

        it("should correctly group bits from a string", function() {
            const container = { input: { string: "hello" } };
            const transform = { out: "(: bit_group($input.string 5) :)" };
            const result = DTL.apply(container, transform);

            assert.deepEqual(result, [13, 1, 18, 22, 24, 27, 3, 15]);
        });

        it("should correctly group bits from a Buffer", function() {
            const container = { input: { buffer: Array.from(new Uint8Array(Buffer.from("hello"))) } };
            const transform = { out: "(: bit_group($input.buffer 5) :)" };
            const result = DTL.apply(container, transform);
            
            assert.deepEqual(result, [ 13, 1, 18, 22, 24, 27, 3, 15 ]);
        });
    });

    describe('remap function', function() {
        it("should correctly remap bit groups using the base32 alphabet", function() {
            const container = { input: { bitGroups: [13, 1, 6, 5, 27, 14, 28, 3] } };
            const transform = { out: "(: remap($input.bitGroups 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567') :)" };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, ['N', 'B', 'G', 'F', '3', 'O', '4', 'D']);
        });
        it("should correctly remap using an object alphabet", function() {
            const alphabet_array = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
            const alphabet = {};
            // make a ROT13 alphabet:
            for ( let i = 0; i < alphabet_array.length; i++) {
                let new_index = i+13;
                if (new_index > 25) {
                    new_index = new_index - 26;
                }
                alphabet[alphabet_array[i]] = alphabet_array[new_index];
            }

            const container = { 
                input: { string: "HELLO" },
                alphabet: alphabet 
            };
            const transform = { out: "(: remap(explode($input.string) $alphabet) :)" };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, ['U', 'R', 'Y', 'Y', 'B' ]);
        });

        it("should return undefined for unmapped values", function() {
            const container = { input: { bitGroups: [13, 1, 256] } };
            const transform = { out: "(: remap($input.bitGroups 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567') :)" };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, ['N', 'B', undefined]);
        });
        it("should correctly use transform to remap", function() {
            const container = { input: { bitGroups: [13, 1, 256] } };
            const transform = { 
                out: "(: remap($input.bitGroups 'timestwo') :)",
                "timestwo": "(: $. * 2 :)"
            };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, [26, 2, 512]);
        });
        it("should correctly use inline transform to remap", function() {
            const container = { input: { bitGroups: [13, 1, 256] } };
            const transform = { 
                out: "(: remap($input.bitGroups '(: $. * 3 :)') :)"
            };
            const result = DTL.apply(container, transform);
            assert.deepEqual(result, [39, 3, 768]);
        });
    });

    describe('Combined bit_group and remap for base32 encoding', function() {
        it("should correctly encode a string to base32", function() {
            const container = { input: { string: "hello" } };
            const transform = { out: "(: remap(bit_group($input.string 5) 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567') :)" };
            const result = DTL.apply(container, transform);
            assert.strictEqual(result.join(''), 'NBSWY3DP');
        });
    });

});
