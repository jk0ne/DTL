const assert = require('assert');
const DTL = require("../lib/DTL.js");

let default_iterations = process.env.DTL_TEST_RANDOM_ITERATIONS ?? 10000;

describe('Random number generation', function() {
    
    describe('rand() helper', function() {
        it("should create numbers that are always positive.", function() {
            let input = { max: 100 };
            const transform = { out: "(: rand($max) :)" };
            for ( let i = 0; i < default_iterations ; i++) {
                const result = DTL.apply(input, transform);
                assert.ok(result >= 0);
            }
        });
    });

    describe('random() helper', function() {
        it("should create numbers that are always positive.", function() {
            let input = {};
            const transform = { out: "(: random() :)" };
            for ( let i = 0; i < default_iterations ; i++) {
                const result = DTL.apply(input, transform);
                assert.ok(result >= 0);
            }
        });
    });

    describe('random_bytes()', function() {
        it("should create an array of bytes of the correct size", function() {
            let input = { size: default_iterations };
            const transform = { out: "(: random_bytes($size) :)" };
            const result = DTL.apply(input, transform);
            assert.equal(result.length, default_iterations);
            for ( let i = 0; i < default_iterations ; i++) {
                assert.ok(result[i] >= 0);
                assert.ok(result[i] <= 255);
            }
        });
    });

});
