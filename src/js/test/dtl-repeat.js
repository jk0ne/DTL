const chai = require('chai');
const assert = require('assert');
const DTL = require('../lib/DTL.js');

describe('repeat() function tests', function() {
  it('should repeat transformation for specified iterations', function() {
    const input = 10;
    const result = DTL.apply(input, `(: repeat(3 $. '(: $. + 5 :)') :)`);
    assert.equal(result, 25);
  });

  it('should respect break_transform and terminate early', function() {
    const input = 1;
    const result = DTL.apply(input, `(: repeat(10 $. '(: $. * 2 :)' '(: $. >= 16 :)') :)`);
    assert.equal(result, 16);
  });

  it('should correctly use @iteration variable', function() {
    const input = 0;
    const result = DTL.apply(input, `(: repeat(5 $. '(: to_json($.) + @iteration :)') :)`);
    assert.equal(result, 10);
  });

  it('scalars as root input are handled correctly', function() {
    const input = 0;
    const result = DTL.apply(input, `(: $. :)`);
    assert.strictEqual(result, 0);
  });

  it('should not overwrite existing $iteration key', function() {
    const input = { "result": 100 };
    const result = DTL.apply(input, `(: repeat(6 $. '(: { ("result": $.result + @iteration * 10) }:)') :)`);
    assert.deepEqual(result, { "result": 250 });
  });

  it('should handle complex data structures', function() {
    const input = { "count": 1, "data": [10, 20] };
    const result = DTL.apply(input, `(: repeat(2 $. '(: { ("count": $.count + 1) ( "data": map($.data "(: $item * 2 :)")) } :)') :)`);
    assert.deepEqual(result, { "count": 3, "data": [40, 80] });
  });
});

