const fs = require('fs');

// Function to read functions from a JSON file
function readFunctionsFromFile(filePath) {
    try {
        const fileContent = fs.readFileSync(filePath, 'utf8');
        return JSON.parse(fileContent);
    } catch (error) {
        console.error('Error reading the file:', error);
        return [];
    }
}

// Path to your JSON file containing the functions
const filePath = 'syntax-list.json'; // Replace with your file path

// Read the functions from the file
const functions = readFunctionsFromFile(filePath);

function createSublimeSnippets(functions) {
    let all_functions = [
`global !p
def is_it_dtl():
    syntax_name = vim.eval("synIDattr(synID(line('.'), col('.'), 1), 'name')")
    return re.search(r'dtl', syntax_name) is not None
endglobal`
    ];
    functions.forEach((func) => {
        let [funcName, paramsPart] = func.split('(');
        const params = paramsPart.slice(0, -1).replace(/\$/g, '\\$').replace(/\[\s+/g, '[').replace(/\s+\]/g, ']');
        const requiredParams = params.trim().split(' ').filter(Boolean);
        let file_prefix = funcName;
        if (file_prefix.length ==1) {
            file_prefix = 'the_' + file_prefix;
        }
        
        let cleaned_params = [];
        for(let i = 0; i < requiredParams.length; i++) {
            if (requiredParams[i].includes('[') && !requiredParams[i].includes(']')) {
                // we have to walk forward until we find a closing one.
                let new_param = requiredParams[i];
                for( let j = i+1; j < requiredParams.length; j++) {
                    new_param += ' '+requiredParams[j];
                    i++;
                    if (requiredParams[j].includes(']')) {
                        cleaned_params.push(new_param);
                        break;
                    }
                }
            } else {
               cleaned_params.push(requiredParams[i]);
            }
        }


        // Create the snippet body
        let snippetBody = `${funcName.trim()}(`;
        cleaned_params.forEach((param, index) => {
            snippetBody += `\${${index + 1}:${param}} `;
        });
        snippetBody = snippetBody.trim();
        snippetBody += ')';

        // XML content for the snippet
        const snippet_text = `snippet ${funcName.trim()} "${funcName.trim()}" "is_it_dtl()" e
${snippetBody}
endsnippet`;
        all_functions.push(snippet_text);
        // Write the snippet to a file
    });
    fs.writeFileSync(`dtl.snippets`, all_functions.join('\n\n'));
}

// Create the Sublime Text snippets
createSublimeSnippets(functions);

console.log('vim snippets generated and placed in dtl.snippets.');
