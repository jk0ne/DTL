const fs = require('fs');

// Function to read functions from a JSON file
function readFunctionsFromFile(filePath) {
    try {
        const fileContent = fs.readFileSync(filePath, 'utf8');
        return JSON.parse(fileContent);
    } catch (error) {
        console.error('Error reading the file:', error);
        return [];
    }
}

// Path to your JSON file containing the functions
const filePath = 'syntax-list.json';

// Read the functions from the file
const functions = readFunctionsFromFile(filePath);

function createSnippets(functions) {
    const snippets = {};
    for (const func of functions) {
        // Extract the function name and parameters
        const [funcName, paramsPart] = func.split('(');
        const params = paramsPart.slice(0, -1).replace(/\$/g, '\\$').replace(/\[\s+/g, '[').replace(/\s+\]/g, ']');
        const requiredParams = params.trim().split(' ').filter(Boolean);

        let cleaned_params = [];
        for(let i = 0; i < requiredParams.length; i++) {
            if (requiredParams[i].includes('[') && !requiredParams[i].includes(']')) {
                // we have to walk forward until we find a closing one.
                let new_param = requiredParams[i];
                for( let j = i+1; j < requiredParams.length; j++) {
                    new_param += ' '+requiredParams[j];
                    i++;
                    if (requiredParams[j].includes(']')) {
                        cleaned_params.push(new_param);
                        break;
                    }
                }
            } else {
               cleaned_params.push(requiredParams[i]);
            }
        }

        // Create the snippet body
        let snippetBody = `${funcName.trim()}(`;
        let paramCounter = 1;
        let separator = ''
        for (const param of cleaned_params) {
            // Required parameter
            snippetBody += separator + `\${${paramCounter}:${param.trim()}}`;
            separator = ' ';
            paramCounter++;
//            snippetBody += paramCounter !== params.length ? ' ' : '';
        }
        snippetBody += ')';

        // Add the snippet to the collection
        snippets[funcName.trim()] = {
            "scope": "DTL",
            "prefix": funcName.trim(),
            "body": snippetBody,
            
            "description": `Snippet for ${funcName.trim()}`
        };
    }
    return snippets;
}

// Create the snippets
const snippets = createSnippets(functions);

// Convert the snippets to JSON
const snippetsJSON = JSON.stringify(snippets, null, 4);

// Save to a file (optional)
fs.writeFileSync('dtl-snippets.json', snippetsJSON);

console.log('Snippets generated and saved as dtl-snippets.json');
