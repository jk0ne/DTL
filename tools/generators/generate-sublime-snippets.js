const fs = require('fs');

// Function to read functions from a JSON file
function readFunctionsFromFile(filePath) {
    try {
        const fileContent = fs.readFileSync(filePath, 'utf8');
        return JSON.parse(fileContent);
    } catch (error) {
        console.error('Error reading the file:', error);
        return [];
    }
}

// Path to your JSON file containing the functions
const filePath = 'syntax-list.json'; // Replace with your file path

// Read the functions from the file
const functions = readFunctionsFromFile(filePath);

function createSublimeSnippets(functions) {
    functions.forEach((func) => {
        let [funcName, paramsPart] = func.split('(');
        const params = paramsPart.slice(0, -1).replace(/\$/g, '\\$').replace(/\[\s+/g, '[').replace(/\s+\]/g, ']');
        const requiredParams = params.trim().split(' ').filter(Boolean);
        let file_prefix = funcName;
        if (file_prefix.length ==1) {
            file_prefix = 'the_' + file_prefix;
        }
        
        let cleaned_params = [];
        for(let i = 0; i < requiredParams.length; i++) {
            if (requiredParams[i].includes('[') && !requiredParams[i].includes(']')) {
                // we have to walk forward until we find a closing one.
                let new_param = requiredParams[i];
                for( let j = i+1; j < requiredParams.length; j++) {
                    new_param += ' '+requiredParams[j];
                    i++;
                    if (requiredParams[j].includes(']')) {
                        cleaned_params.push(new_param);
                        break;
                    }
                }
            } else {
               cleaned_params.push(requiredParams[i]);
            }
        }


        // Create the snippet body
        let snippetBody = `${funcName.trim()}(`;
        cleaned_params.forEach((param, index) => {
            snippetBody += `\${${index + 1}:${param}} `;
        });
        snippetBody = snippetBody.trim();
        snippetBody += ')';

        // XML content for the snippet
        const snippetXML = `<snippet>
    <content><![CDATA[
${snippetBody}
]]></content>
    <tabTrigger>${funcName.trim()}</tabTrigger>
    <scope>meta.block.dtl</scope> <!-- Adjust the scope as necessary -->
    <description>Snippet for ${funcName.trim()}</description>
</snippet>`;

        // Write the snippet to a file
        fs.writeFileSync(`${file_prefix.trim()}.sublime-snippet`, snippetXML);
    });
}

// Create the Sublime Text snippets
createSublimeSnippets(functions);

console.log('Sublime Text snippets generated.');
