# Enabling DTL Syntax Highlighting

The DTL (Data Transformation Language) syntax can be integrated into JSON, YAML
and Javascript files within Vim by including specific syntax highlighting
functions. This guide explains how to set up your Vim environment to recognize
DTL expressions within JSON and YAML files. It also provides information on how
to load the dtl.snippets file (UltiSnips is required) for all DTL helpers.

## Functions for DTL Syntax Inclusion

Three functions are defined in your Vim configuration to facilitate the
inclusion of DTL syntax in different file types:

### JSON Files

```vim
" Function to include DTL syntax in JSON strings
function! IncludeDTLinJSON()
    syntax include @DTLSyntax syntax/dtl.vim
    syntax region dtl_expression matchgroup=dtl_happy_tags start=+"(:+ skip=/\\"/ end=+:)\ze"+ containedin=jsonString oneline
    " uncomment the following line if you use ultisnips and want to use the dtl snippets
    "UltiSnipsAddFiletypes dtl.json
endfunction
```

- This function includes DTL syntax in JSON files.
- DTL expressions are highlighted within JSON strings.

### YAML Files

```vim
" Function to include DTL syntax in YAML strings
function! IncludeDTLinYAML()
    syntax include @DTLSyntax syntax/dtl.vim
    syntax region dtl_expression matchgroup=dtl_happy_tags start=+"\zs(:+ skip=/\\"/ end=+:)\ze"+ containedin=yamlFlowString
    syntax region dtl_expression matchgroup=dtl_happy_tags start=+'(:+ skip=/\\'/ end=+:)\ze'+ containedin=yamlFlowString
    " uncomment the following line if you use ultisnips and want to use the dtl snippets
    " UltiSnipsAddFiletypes dtl.yaml
endfunction
```

- This function includes DTL syntax in YAML files.
- DTL expressions are highlighted within YAML strings.

### JavaScript Files

```vim
" Function to include DTL syntax in Javascript strings
function! IncludeDTLinJavascript()
    syntax include @DTLSyntax syntax/dtl.vim
    syntax region dtl_expression matchgroup=dtl_happy_tags start=+"\zs(:+ end=+:)\ze"+ containedin=jsString oneline
    syntax region dtl_expression matchgroup=dtl_happy_tags start=+'\zs(:+ end=+:)\ze'+ containedin=jsString oneline
    " uncomment the following line if you use ultisnips and want to use the dtl snippets
    " UltiSnipsAddFiletypes dtl.javascript
endfunction
```

- This function includes DTL syntax in JavaScript files.
- DTL expressions are highlighted within JavaScript strings.

## Autocommands for Syntax Inclusion

The following autocommands in your Vim configuration trigger the above
functions based on the file type:

```vim
autocmd FileType json call IncludeDTLinJSON()
autocmd FileType javascript call IncludeDTLinJavascript()
autocmd FileType yaml call IncludeDTLinYAML()
```

- These autocommands automatically call the respective functions when a file of
  type JSON, JavaScript, or YAML is opened.

## Custom Filetype Assignments

Additionally, the configuration includes custom filetype assignments for files
with `.transform` and `.tx` extensions:

```vim
au BufNewFile,BufRead *.transform setfiletype json
au BufNewFile,BufRead *.tx setfiletype json
```

- These commands set the filetype of `.transform` and `.tx` files to JSON,
  enabling JSON (and by extension, DTL) syntax highlighting for these file
types.

Ensure you have the `dtl.vim` syntax file correctly installed in your Vim
syntax directory (typically `~/.vim/syntax/`) for this setup to work. After
updating your Vim configuration, you might need to restart Vim or source your
`.vimrc` file for the changes to take effect.


## Installing DTL Snippets for UltiSnips in Vim

### Prerequisites

Before you can use DTL snippets, you must have UltiSnips installed in Vim. If
you don't have UltiSnips installed, you can find it on
[VimAwesome](https://vimawesome.com/plugin/ultisnips). To install UltiSnips
using a plugin manager like [Vim-Plug](https://github.com/junegunn/vim-plug),
add the following line to your `.vimrc` or `init.vim`:

```vim
Plug 'SirVer/ultisnips'
```

Then, run `:PlugInstall` in Vim to install the plugin.

### Installing DTL Snippets

Once UltiSnips is installed, follow these steps to install the DTL snippets:

1. **Download the `dtl.snippets` File:**
   Download the `dtl.snippets` file from the repository or copy its contents.

2. **Place the File in the UltiSnips Directory:**
   Place the `dtl.snippets` file in your UltiSnips directory. The default
   location is `~/.vim/UltiSnips/`. If the directory doesn't exist, create it:

   ```bash
   mkdir -p ~/.vim/UltiSnips/
   ```

3. **Add the File to the UltiSnips Directory:**
   Copy the `dtl.snippets` file to this directory. You can do this by running:

   ```bash
   cp /path/to/dtl.snippets ~/.vim/UltiSnips/
   ```

   Replace `/path/to/dtl.snippets` with the actual path to the downloaded
   `dtl.snippets` file.

4. **Verify Installation:**
   Open Vim and try using one of the DTL snippets in a supported file (JSON,
   JavaScript, or YAML) within a DTL region. The snippet should trigger only when
   the cursor is inside a DTL region.

### Note

- The DTL snippets are configured to trigger only within DTL regions to avoid
  conflicts with other snippets. This makes them safe to use alongside your
  existing UltiSnips configuration.
- If you encounter any issues, ensure that UltiSnips is correctly installed and
  that the `dtl.snippets` file is placed in the correct directory.
