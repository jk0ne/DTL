" Vim syntax file for dtl
" Language: dtl - Data Transformation Language
" Maintainer: Jay Kuri
" Latest Revision: 2023-11-25

"if exists("b:current_syntax")
"  finish
"endif
syntax case match

syn cluster dtlCommonElements contains=dtl_variable,dtl_operators,dtl_numbers,dtl_boolean,dtl_undef,dtl_helper
syn cluster dtlContainers contains=dtl_expression,dtl_nested_expression,dtl_block,dtl_helper

syn region dtl_expression matchgroup=dtl_happy_tags start=+"(:+ skip=/\\"/ end=+:)"+ contains=@dtlCommonElements 
syn region dtl_nested_expression matchgroup=dtl_sub_happy_tags start=+'\zs(:+ end=+:)\ze'+ contained contains=@dtlCommonElements containedin=dtl_strings 
syn region dtl_nested_expression matchgroup=dtl_sub_happy_tags start=+`\zs(:+ end=+:)\ze`+ contained contains=@dtlCommonElements containedin=dtl_strings 

" Define strings that can contain nested expressions
syn region dtl_single_strings start="'" skip="\\'" end="'" contains=dtl_nested_expression contained containedin=@dtlContainers
syn region dtl_single_strings start="`" skip="\\`" end="`" contains=dtl_nested_expression contained containedin=@dtlContainers
syn region dtl_double_strings start=+"+ skip=+\\"+ end=+"+ contains=dtl_nested_expression contained containedin=dtl_single_strings 

syn match dtl_transform_name /\v(\-\>)\s*['`]?[a-zA-Z0-9_\.]+['`]?/ contained containedin=@dtlContainers
syn match dtl_transform_arrow /\v(\-\>)/ contained containedin=dtl_transform_name

syn match dtl_regex_literal /\v\/[^\/]+\/[dgimsuvy]*/ contained containedin=@dtlContainers
"syn match dtl_regex_flags "[dgimsuvy]\+" contained containedin=dtl_regex_delimiter

syn match dtl_variable /\$[a-zA-Z0-9_\.\[\]]\+/ contained containedin=@dtlContainers
syn match dtl_operators /\v([^><!+\-\/*\|&\s]|^)\zs[><!+\-\/*\|&]\ze([^><!+\-\/*\|&\s])/ contained containedin=@dtlContainers
"syn match dtl_operators /\v[><!+\-\/*\|&]/ contained containedin=@dtlContainers
syn match dtl_operators /\v(\|\|)(&&)/ contained containedin=@dtlContainers
syn match dtl_operators /\v[\!]?\=[~><]/ contained containedin=@dtlContainers
syn match dtl_number_float /\v[+\-]?(\d+\.\d*|\.\d+)([eE][+\-]?\d+)?/ contained containedin=@dtlContainers
syn match dtl_number_int /\v[+\-]?[0-9]+/ contained containedin=@dtlContainers
syn match dtl_number_int /\v[+\-]?[0-9]+/ contained containedin=@dtlContainers
syn match dtl_number_alternate_base /\v0(o[0-7]+|b[01]+|x[\da-fA-F]+)/ contained containedin=@dtlContainers

syn keyword dtl_boolean true false contained containedin=@dtlContainers
syn keyword dtl_undef undef undefined contained containedin=@dtlContainers
" flag variable usage inside constant strings 
" syn match dtl_illegal_variable /\$[a-zA-Z0-9_\.\[\]]\+/ containedin=dtl_strings
syn region dtl_helper matchgroup=dtl_function_name start=/[a-zA-Z0-9_#&?@\\\^]\+(/ end=/)/ contained containedin=@dtlContainers 

syn region dtl_block matchgroup=dtl_parens start="(" skip="|.\{-}|" end=")" transparent skipwhite skipnl fold contained containedin=@dtlContainers
syn region dtl_block matchgroup=dtl_parens start="{" skip="|.\{-}|" end="}" transparent skipwhite skipnl fold contained containedin=@dtlContainers
syn region dtl_block matchgroup=dtl_parens start="\[" skip="|.\{-}|" end="\]" transparent skipwhite skipnl fold contained containedin=@dtlContainers

syn region dtl_comment start=/\/\// end=/$/ contains=@Spell oneline
syn region dtl_comment start=/\/\*/  end=/\*\// contains=@Spell fold 

highlight link dtl_happy_tags Delimiter
highlight link dtl_sub_happy_tags Macro
highlight link dtl_expression String
highlight link dtl_single_strings String
highlight link dtl_double_strings String
highlight link dtl_regex_literal Constant
highlight link dtl_regex_delimiter Special
highlight link dtl_regex_flags Type
highlight link dtl_comment Comment
highlight link dtl_parens Delimiter
highlight link dtl_transform_shortcut Error

highlight link dtl_openingTag Comment
highlight link dtl_closingTag Comment
highlight link dtl_dtl Keyword
highlight link dtl_block Todo
highlight link dtl_variable Identifier
highlight link dtl_operators Operator
highlight link dtl_undef Keyword
highlight link dtl_boolean Boolean
highlight link dtl_number_int Number
highlight link dtl_number_alternate_base Number
highlight link dtl_number_float Float
" highlight link dtl_illegal_variable Error
highlight link dtl_function_name Function
highlight link dtl_helper Function
highlight link dtl_transform_name Macro
highlight link dtl_transform_arrow Delimiter

let b:current_syntax = "dtl"
