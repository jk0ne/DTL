# &( $data_item1 $data_item2 [ $data_item3 ... ] )

Concatenates the passed items together. The type of the returned item is determined by the first argument.

- If the first argument is a list, the remaining items will be appended to that list.
- If the first argument is a string, the subsequent items will be converted to strings (if necessary) and concatenated.
- If the first argument is an object, and the additional items are also objects, the returned item will be the merged objects at the top level.

## Returns:
The items passed as arguments concatenated together.

## Examples:

- `&([] 38)` will produce `[38]`.
- `&([] [38])` will also result in `[38]`.
- `&({} {"a": 1} {"b": 2})` will merge the objects to `{"a": 1, "b": 2}`.

## Usage Notes:

- This function is particularly useful when you need to ensure a consistent input type for subsequent operations. For instance, to normalize a variable input (`$`) that could be an object or an array of objects, use `&([], $)` to wrap a single object in an array or to maintain an existing array.

- When working with data that might have variable structures, such as API responses that return either an object or an array of objects, wrapping the response in an array using `&([], response)` will simplify the downstream data handling by providing a consistent format to iterate over or manipulate.

- It's a handy tool for pre-processing input for functions that require a specific data type, ensuring that the rest of your data pipeline has a predictable starting point.

Remember that when merging objects, only top-level properties are considered. Nested objects are not recursively merged and may be overwritten if they share the same key.

## Advanced Tips:

- When concatenating objects, you may encounter key collisions. In such cases, the last object's value for that key will take precedence.

- To create an array of arrays or an array of objects, ensure your first argument is an empty array, followed by your arrays or objects enclosed in additional brackets: `&([], [array1], [array2])`.

Feel free to experiment with `&()` to familiarize yourself with its behavior in various scenarios, which can be a powerful feature in your DTL toolkit.
