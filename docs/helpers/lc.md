## `lc()` Function

### Description
The `lc()` function transforms the provided string into all lowercase characters. It is utilized for formatting purposes, ensuring consistency in case throughout strings where lowercase is needed.

### Usage
```
lc( $string )
```
- `$string`: The string to be converted to lowercase.

### Details
- All characters in the string that have uppercase equivalents will be converted to lowercase.
- The function generates a new string with the lowercase conversion applied and does not alter the original string.

### Example
```dtl
lc("Hello World")
```
This will return:
```
"hello world"
```

### Considerations
- If the string contains characters without lowercase equivalents (like numbers or symbols), those characters will remain unchanged.
- The `lc()` function expects a string input. Non-string inputs will not be converted and may lead to unexpected results.

### Performance Implications
- As a straightforward transformation, the `lc()` function has a low impact on performance and can be safely used in large-scale data transformations or in scenarios requiring frequent case adjustments.

### Note
- This function is particularly useful for normalizing case-sensitive data inputs, allowing for more accurate comparisons and searches when uniform case is essential.
