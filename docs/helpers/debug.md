## `@()` Function

### Description
The `@()` function is used for debugging purposes. It outputs the value of the data item to the debug console without modifying the item. It's primarily used to inspect values during the transformation process.

### Usage
```
@( [label] $data_item )
```
- `$data_item`: The item to output to the debug console. This can be any data type.
- `label` *(optional)*: A string to prefix the output on the console for easy identification.

### Details
- By default, the output is in a human-readable format, where complex data types are JSON-encoded.
- The function can also be called with a `label` argument to prefix the output, aiding in distinguishing multiple debug outputs.
- The debug handler can be customized via DTL options to format the output as needed for the application.
- The output is intended for development and debugging purposes and typically appears in the console.

### Example
```dtl
@( "User Data:" $user )
```
This will output to the console something like:
```
User Data: {"name":"John Doe","age":30}
```

### Considerations
- The `@()` function can be used anywhere within a transform where a data item is valid.
- It simply returns the passed value, allowing it to be inserted into a transform without affecting the data flow.
- When debugging with large data sets, be aware that the output will be proportional to the size of the data, which might lead to performance implications if the volume is significant.
- It's recommended to remove `@()` function calls from the transform once debugging is complete and before moving into a production environment.

### Performance Implications
- The function involves the JSON stringification of the data item and its output to the console, which is a low-overhead operation for small to moderate-sized data.
- Caution is advised when using `@()` with very large data items, as the stringification and output process may become intensive and affect performance.

### Production Use
- The `@()` function is intended for debugging during development. It is advisable to remove such calls before deploying the transform in a production environment to avoid unnecessary performance overhead and potential exposure of sensitive data in log files.

