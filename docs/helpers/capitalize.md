## `capitalize()` Function

### Description
The `capitalize()` function modifies the first character of the provided string, converting it to uppercase. It is useful for formatting strings where a capital letter is required at the beginning.

### Usage
```
capitalize( $string )
```
- `$string`: The string to capitalize.

### Details
- Only the first character of the string will be affected. If the first character is already in uppercase, the function will return the string unchanged.
- The function does not modify the original string; it returns a new string with the change applied.

### Example
```dtl
capitalize("hello world")
```
This will return:
```
"Hello world"
```

### Considerations
- If the first character in the string is not a letter (for instance, a digit or a symbol), the function will not modify it, and the string will be returned as is.
- The `capitalize()` function is designed to handle strings. Passing non-string values will not produce a capitalized output.

### Performance Implications
- The `capitalize()` function is a simple operation with minimal performance overhead, suitable for use even within large datasets or complex transforms.

### Note
- This function is useful for text transformations and should be used when the case of individual words needs to be standardized, particularly at the beginning of sentences or for proper nouns.
