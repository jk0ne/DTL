# Derive

Derive applies a series of conditional transformations to the data based on an action map. An action map is an array of pairs where the first element is a condition and the second element is the transformation to apply if the condition is true.

## Syntax

```dtl
derive($data $action_map)
```

## Parameters

- `$data`: The data to be processed.
- `$action_map`: An array of pairs, each containing a condition and a corresponding transformation.

## Description

When `derive` is called, it processes the data against each condition in the action map in order. For each element of the data, `derive` checks the conditions in the action map sequentially until it finds a condition that evaluates to true. Once a true condition is found, `derive` applies the corresponding transformation to the data element and stops processing further conditions for that element.

If no conditions are met, the result is `undefined`.

## Examples

In the following example, `derive` is used within a larger transformation that filters (`grep`) through an array of words (`$.words`). Each word is processed through an action map (`derivation_map`). The conditions test for specific patterns or types, and depending on the result of the test, different transformations are applied:

```dtl
{
    "out": "(: grep(map($.words `(: derive($. 'derivation_map') :)` )) :)",
    "derivation_map": [
        [ "(: $item =~ /z/ :)", "(: 'has_z' :)" ],
        [ "(: $item =~ /[aeiou]{2}/ :)", "(: 'double' :)"],
        [ "(: $item =~ /b/ :)", "(: 'has_b' :)" ],
        [ "(: typeof($item) == 'number' :)", "(: undefined :)" ],
        [ "(: typeof($item) == 'array' :)", "array_handler"],
        [ "(: true :)", "(: $item :)"]
    ],
    "array_handler": "(: reduce($item '(: $memo + $item :)') :)"
}
```

This map defines transformations based on the presence of specific characters ('z', 'b'), a double vowel pattern, the type of the item, and a default case. For arrays, a separate transformation is referenced (`array_handler`), which performs a reduction on the array elements.

## Returns

The transformed data according to the first matching condition from the action map or `undefined` if no condition matches.
